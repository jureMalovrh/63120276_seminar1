﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_63120276_Seminar1.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <hr />
        <asp:Label ID="Label2" runat="server" Font-Size="Larger" ForeColor="#006600" Text="Oddelki in skupine izdelkov"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label4" runat="server" Text="F2(delno), F4"></asp:Label>
        <br />
        <asp:DropDownList ID="DDL_Categories" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource8" DataTextField="Name" DataValueField="ProductCategoryID" Height="20px" Width="204px" BackColor="Lime" OnSelectedIndexChanged="DDL_Categories_SelectedIndexChanged1">
        </asp:DropDownList>
        <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT ProductCategoryID, Name FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID IS NULL)"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource9" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Name, ProductCategoryID FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDL_Categories" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <asp:GridView ID="GV_Categories" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="White" DataKeyNames="ProductCategoryID" DataSourceID="SqlDataSource9" GridLines="None" SelectedIndex="0" Width="199px" OnSelectedIndexChanged="GV_Categories_SelectedIndexChanged">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Name" SortExpression="Name" HeaderText="Name"></asp:BoundField>
                <asp:CommandField ShowSelectButton="True" ButtonType="Button" SelectText="&gt;" />
            </Columns>
            <SelectedRowStyle BackColor="#66FF66" BorderColor="#003300" BorderStyle="Solid" BorderWidth="2px" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource10" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT SalesLT.ProductModelProductDescription.ProductModelID, SalesLT.ProductDescription.Description,  SalesLT.ProductModel.Name FROM SalesLT.Product INNER JOIN SalesLT.ProductModel ON SalesLT.Product.ProductModelID = SalesLT.ProductModel.ProductModelID INNER JOIN SalesLT.ProductModelProductDescription ON SalesLT.ProductModel.ProductModelID = SalesLT.ProductModelProductDescription.ProductModelID INNER JOIN SalesLT.ProductDescription ON SalesLT.ProductModelProductDescription.ProductDescriptionID = SalesLT.ProductDescription.ProductDescriptionID WHERE (SalesLT.ProductModelProductDescription.Culture = N'en') AND (SalesLT.Product.ProductCategoryID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <hr />
        <asp:Label ID="Label1" runat="server" Font-Size="Larger" ForeColor="#006600" Text="Modeli"></asp:Label>
        <asp:GridView ID="GV_Models" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ProductModelID" DataSourceID="SqlDataSource10" GridLines="None" PageSize="5" SelectedIndex="0" Width="637px" OnSelectedIndexChanged="GV_Models_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="Name" SortExpression="Name" HeaderText="Name"></asp:BoundField>
                <asp:BoundField DataField="Description" SortExpression="Description" HeaderText="Description" />
                <asp:CommandField ShowSelectButton="True" SelectText="Prikaži izdelke" />
            </Columns>
            <RowStyle BorderStyle="None" />
            <SelectedRowStyle BackColor="#66FF66" BorderColor="#003300" BorderStyle="Solid" BorderWidth="3px" />
        </asp:GridView>
        <br />
        <asp:SqlDataSource ID="SqlDataSource11" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT ProductNumber, Name, Color, Size, ListPrice, ProductID, ThumbNailPhoto, ThumbnailPhotoFileName FROM SalesLT.Product WHERE (ProductModelID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Models" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <hr />
        <br />
        <asp:Label ID="Label3" runat="server" Font-Size="Larger" ForeColor="#006600" Text="Izdelki"></asp:Label>
        <asp:GridView ID="GV_Products" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource11" GridLines="None" OnSelectedIndexChanged="GridView3_SelectedIndexChanged" Width="641px" DataKeyNames="ProductID">
            <Columns>
                <asp:TemplateField> <ItemTemplate> <img src='data:image/jpg;base64,<%# Convert.ToBase64String((byte[])Eval("ThumbNailPhoto")) %>' /> </ItemTemplate> </asp:TemplateField>
                <asp:BoundField DataField="ProductNumber" HeaderText="šifra izdelka" SortExpression="ProductNumber">
                </asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Naziv izdelka" SortExpression="Name">
                </asp:BoundField>
                <asp:BoundField DataField="Size" HeaderText="velikost" SortExpression="Size">
                </asp:BoundField>
                <asp:BoundField DataField="Color" HeaderText="barva" SortExpression="Color">
                </asp:BoundField>
                <asp:BoundField DataField="ListPrice" HeaderText="cena" SortExpression="ListPrice" DataFormatString="{0:c}">
                </asp:BoundField>
                <asp:CommandField SelectText="podrobnosti" ShowSelectButton="True" />
            </Columns>
            <SelectedRowStyle BackColor="#66FF66" BorderColor="#003300" BorderStyle="Solid" />
        </asp:GridView>
        <br />
        <br />
        <asp:Label ID="L_Product" runat="server" ForeColor="#006600" Text="Podatki o izdelku" Visible="False"></asp:Label>
        <br />
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Color AS Barva, Size AS Velikost, Weight AS Teža, StandardCost AS [Nabavna cena], ListPrice AS [Prodajna cena], Name, ProductID FROM SalesLT.Product WHERE (ProductID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:DetailsView ID="DV_Product" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource5" GridLines="None" Height="50px" Visible="False" Width="168px" DataKeyNames="ProductID">
            <Fields>
                <asp:BoundField DataField="ProductID" HeaderText="Šifra izdelka" SortExpression="ProductID" InsertVisible="False" ReadOnly="True" />
                <asp:BoundField DataField="Name" HeaderText="Naziv" SortExpression="Name" />
                <asp:BoundField DataField="Barva" HeaderText="Barva" SortExpression="Barva" />
                <asp:BoundField DataField="Velikost" HeaderText="Velikost" SortExpression="Velikost" />
                <asp:BoundField DataField="Teža" HeaderText="Teža" SortExpression="Teža" />
                <asp:BoundField DataField="Nabavna cena" HeaderText="Nabavna cena" SortExpression="Nabavna cena" />
                <asp:BoundField DataField="Prodajna cena" HeaderText="Prodajna cena" SortExpression="Prodajna cena" />
            </Fields>
        </asp:DetailsView>
        <br />
        <asp:Label ID="L_Orders" runat="server" ForeColor="#006600" Text="Seznam naročil" Visible="False"></asp:Label>
        <br />
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.SalesOrderDetail.SalesOrderID, SalesLT.SalesOrderHeader.OrderDate, SalesLT.Customer.CompanyName + ' (' + SalesLT.Address.City + ' ' + SalesLT.Address.CountryRegion + ' )' AS Kupec, SalesLT.SalesOrderDetail.OrderQty, SalesLT.SalesOrderDetail.UnitPrice, SalesLT.SalesOrderDetail.UnitPriceDiscount, SalesLT.SalesOrderDetail.LineTotal FROM SalesLT.SalesOrderDetail INNER JOIN SalesLT.SalesOrderHeader ON SalesLT.SalesOrderDetail.SalesOrderID = SalesLT.SalesOrderHeader.SalesOrderID INNER JOIN SalesLT.Customer ON SalesLT.SalesOrderHeader.CustomerID = SalesLT.Customer.CustomerID INNER JOIN SalesLT.Address ON SalesLT.SalesOrderHeader.ShipToAddressID = SalesLT.Address.AddressID AND SalesLT.SalesOrderHeader.BillToAddressID = SalesLT.Address.AddressID INNER JOIN SalesLT.Product ON SalesLT.SalesOrderDetail.ProductID = SalesLT.Product.ProductID WHERE (SalesLT.Product.ProductID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:GridView ID="GV_Orders" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource6" Visible="False" DataKeyNames="SalesOrderID" OnSelectedIndexChanged="GV_Orders_SelectedIndexChanged" GridLines="None">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="OrderDate" HeaderText="Datum" SortExpression="OrderDate" DataFormatString="{0:d}">
                </asp:BoundField>
                <asp:BoundField DataField="Kupec" HeaderText="Kupec" ReadOnly="True" SortExpression="Kupec">
                </asp:BoundField>
                <asp:BoundField DataField="OrderQty" HeaderText="Količina" SortExpression="OrderQty">
                </asp:BoundField>
                <asp:BoundField DataField="UnitPrice" HeaderText="Cena enote" SortExpression="UnitPrice" DataFormatString="{0:c}">
                </asp:BoundField>
                <asp:BoundField DataField="UnitPriceDiscount" HeaderText="Popust" SortExpression="UnitPriceDiscount" DataFormatString="{0:p}">
                </asp:BoundField>
                <asp:BoundField DataField="LineTotal" HeaderText="Cena" ReadOnly="True" SortExpression="LineTotal" DataFormatString="{0:c}">
                </asp:BoundField>
            </Columns>
        </asp:GridView>
        <br />
        <br />
    
    </div>
    </form>
</body>
</html>
