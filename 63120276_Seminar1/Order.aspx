﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="_63120276_Seminar1.Order" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="idData" runat="server" Text="Label"></asp:Label>
        <br />
        <asp:Label ID="Label2" runat="server" Font-Size="Larger" ForeColor="#006600" Text="Naročilo"></asp:Label>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT OrderDate, DueDate, SalesOrderNumber FROM SalesLT.SalesOrderHeader WHERE (SalesOrderID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="idData" Name="filter" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource1" GridLines="None" Height="77px" style="margin-top: 0px" Width="230px">
            <Fields>
                <asp:BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate" />
                <asp:BoundField DataField="DueDate" HeaderText="DueDate" SortExpression="DueDate" />
                <asp:BoundField DataField="SalesOrderNumber" HeaderText="SalesOrderNumber" SortExpression="SalesOrderNumber" ReadOnly="True" />
            </Fields>
        </asp:DetailsView>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Kupec"></asp:Label>
        <br />
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.Customer.CompanyName, SalesLT.Address.AddressLine1 + ', ' + SalesLT.Address.City + ', ' + SalesLT.Address.PostalCode + ', ' + SalesLT.Address.CountryRegion AS Naslov, SalesLT.Customer.EmailAddress, SalesLT.Customer.Phone FROM SalesLT.Address CROSS JOIN SalesLT.SalesOrderDetail CROSS JOIN SalesLT.Customer WHERE (SalesLT.SalesOrderDetail.SalesOrderID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="idData" Name="filter" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:DetailsView ID="DetailsView2" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource2" Height="50px" Width="125px">
            <Fields>
                <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
                <asp:BoundField DataField="Naslov" HeaderText="Naslov" ReadOnly="True" SortExpression="Naslov" />
                <asp:BoundField DataField="EmailAddress" HeaderText="EmailAddress" SortExpression="EmailAddress" />
                <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
            </Fields>
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.SalesOrderHeader.ShipDate, SalesLT.SalesOrderHeader.ShipMethod, SalesLT.Address.AddressLine1 + ', ' + SalesLT.Address.City + ', ' + SalesLT.Address.PostalCode + ', ' + + ', ' + SalesLT.Address.CountryRegion AS Naslov FROM SalesLT.SalesOrderHeader INNER JOIN SalesLT.Address ON SalesLT.SalesOrderHeader.ShipToAddressID = SalesLT.Address.AddressID AND SalesLT.SalesOrderHeader.BillToAddressID = SalesLT.Address.AddressID WHERE (SalesLT.SalesOrderHeader.SalesOrderID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="idData" Name="filter" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SubTotal, TaxAmt, Freight, TotalDue FROM SalesLT.SalesOrderHeader WHERE (SalesOrderID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="idData" Name="filter" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.Product.ProductNumber, SalesLT.Product.Name, SalesLT.SalesOrderDetail.OrderQty, SalesLT.SalesOrderDetail.UnitPrice, SalesLT.SalesOrderDetail.UnitPriceDiscount, SalesLT.SalesOrderDetail.LineTotal FROM SalesLT.Product INNER JOIN SalesLT.SalesOrderDetail ON SalesLT.Product.ProductID = SalesLT.SalesOrderDetail.ProductID WHERE (SalesLT.SalesOrderDetail.SalesOrderID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="idData" Name="filter" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <br />
    
    </div>
    </form>
</body>
</html>
