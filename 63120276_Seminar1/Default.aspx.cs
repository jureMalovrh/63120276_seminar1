﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _63120276_Seminar1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridView3_SelectedIndexChanged(object sender, EventArgs e)
        {
            int indeks = GV_Products.SelectedIndex;
            if (indeks >= 0) {
                L_Product.Visible = true;
                DV_Product.Visible = true;
                L_Orders.Visible = true;
                GV_Orders.Visible = true;
                
            }
        }

        protected void DDL_Categories_SelectedIndexChanged(object sender, EventArgs e)
        {
            GV_Categories.SelectedIndex = 0;
            GV_Models.SelectedIndex = 0;
            GV_Products.SelectedIndex = -1;
            L_Product.Visible = false;
            DV_Product.Visible = false;
            L_Orders.Visible = false;
            GV_Orders.Visible = false;
        }

        protected void GV_Categories_SelectedIndexChanged(object sender, EventArgs e)
        {
            GV_Models.SelectedIndex = 0;
            GV_Products.SelectedIndex = -1;
            L_Product.Visible = false;
            DV_Product.Visible = false;
            L_Orders.Visible = false;
            GV_Orders.Visible = false;
        }

        protected void GV_Models_SelectedIndexChanged(object sender, EventArgs e)
        {
            GV_Products.SelectedIndex = -1;
            L_Product.Visible = false;
            DV_Product.Visible = false;
            L_Orders.Visible = false;
            GV_Orders.Visible = false;
        }

        protected void GV_Orders_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            String id = GV_Orders.SelectedValue.ToString();
            
            string url = "Order.aspx?id="+id;
            string s = "window.open('" + url + "', 'popup_window', 'width=300,height=100,left=100,top=100,resizable=yes');";
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);

        }

        protected void DDL_Categories_SelectedIndexChanged1(object sender, EventArgs e)
        {
            GV_Categories.SelectedIndex = 0;
            GV_Models.SelectedIndex = 0;
            GV_Products.SelectedIndex = -1;
            L_Product.Visible = false;
            DV_Product.Visible = false;
            L_Orders.Visible = false;
            GV_Orders.Visible = false;
        }

    }
}